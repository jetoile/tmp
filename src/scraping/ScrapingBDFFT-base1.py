# -*- coding: utf-8 -*-
##############################################

# Defintion : 

# ScrapingBDFFT-base1.py est un script extrit de Ipython notebook ayant pour but de Scrapper les information du Finantial Time et de mettre les articles dans le bon format.
# Le scrapping s'effectue toujours le jour même où le script est lancé 

# Usage : 

# python ScrapingBDFFT-base1.py 

# Dependances : 

# Les dépendances sont les mêmes que pour l'autre script de scrapping

###############################################


from bs4 import BeautifulSoup, SoupStrainer
import os, sys
import requests
from requests.exceptions import ConnectionError
import pickle
import lxml

#import MySQLdb
import sqlite3
from unidecode import unidecode
import datetime
from datetime import date
import time

######### modif BPT ############
sys.path.append("../")
import config_bdf

######## Variabiliser

# database_path=config_bdf.database_path
# try:
#    os.chdir(database_path)
# except:
#    raise ValueError("On a besoin de spécifier un chemin d'accès à la base qui existe ")

source_input="FT"
region = ["asia","india","europe","middleeast","uk","us"]
liste_articles_scrap=[]
url_rss="http://www.ft.com/rss/home/"
date_scraping=datetime.date.today()
date_heure_debut=datetime.datetime.now()

# On initialise une connexion avec la base de données pour remplir les premières colonnes #

### On charge la base de données dont on ouvre la connexion
mysql = config_bdf.mysql
db = sqlite3.connect(mysql['sqlite_db'])
try :   
   #db=MySQLdb.connect(host=mysql['host'], user= mysql['user'], db=mysql['db'])
   db = sqlite3.connect(mysql['sqlite_db'])
except:
   raise ValueError("Impossible de se connecter à la base de données, vérifiez les informations de connexions dans config_bdf.py")


try:
    c=db.cursor()
    
    # On cherche l'ID de la source dans la BD #
    c.execute("""SELECT SCE_ID FROM source WHERE source = ('%s') """%source_input)
    try :   
        sce_id = c.fetchone()[0]
    except TypeError :
        print("La source n'est pas connue")
        c.close()
        db.close()
        sys.exit(0)
    
    # On cherche l'ID de la date dans la BD, si on ne la trouve pas on la crée #
    c.execute("""SELECT DAT_ID FROM date WHERE date= ('%s') """%date_scraping)
    try :   
        dat_id = c.fetchone()[0]
    except TypeError :
        print("La date n'existe pas, nous la créons")
        c.execute("""INSERT INTO date (date) VALUES ('%s')"""%date_scraping)
        c.execute("""SELECT DAT_ID FROM date WHERE date= ('%s') """%date_scraping)
        dat_id = c.fetchone()[0]
    
    # On insere la date, la source et l'heure du début du scrapping dans la BD pour avoir un ID du scrapping # 
    debut=datetime.datetime.now()
    c.execute("""INSERT INTO scraping (DAT_ID,SCE_ID,statut,heure_debut,heure_fin) VALUES (%s,%s,'%s','%s','%s')"""%(dat_id,sce_id,'in process',debut,debut))
    
    # On cherche l'ID du scrapping dans la BD#
    c.execute("""SELECT SCP_ID FROM scraping WHERE SCE_ID= (%s) AND DAT_ID= (%s) AND heure_debut= ('%s')  """%(sce_id,dat_id,debut))
    try :   
        scp_id = c.fetchone()[0]
        db.commit()
        c.close()
    except TypeError :
        print('le programme n\'a pas trouver d\'ID pour le travail de scrapping \n Pas d\initialisation de la base')
        c.close()
        db.close()
        sys.exit(0)

except InterfaceError :
    print('Erreur de connexion avec la base de données BDF')
    sys.exit(0)

# On scrappe en premier le flux RSS sous forme XML du FT#
liste_articles=[]
liste_articles2=[]

for i in range(len(region)) :
    try:
        time.sleep(2)
        r = requests.get(url_rss+region[i], proxies={"http":"http://"+config_bdf.proxy_user+":"+config_bdf.proxy_password+"@"+config_bdf.http_proxy})
        time.sleep(2)
        r.raise_for_status()
        soup = BeautifulSoup(r.text,"xml")
        liste_articles= unicode(soup.findAll('guid'))
        liste_articles= liste_articles.replace("</guid>","")
        liste_articles= liste_articles.replace("<guid isPermaLink=\"false\">","")
        liste_articles= liste_articles.replace("[","")
        liste_articles= liste_articles.replace("]","")
        liste_articles= liste_articles.replace(" ","")
        liste_articles= liste_articles.split(",")
        for j in range(len(liste_articles)) :
            pos=liste_articles[j].find("?ftcamp") 
            liste_articles[j]=liste_articles[j][0:pos]
            liste_articles2.append(liste_articles[j])
            
    except requests.exceptions.HTTPError as e:
        print e
        fin=datetime.datetime.now()

        c=db.cursor()
        c.execute("""UPDATE scraping SET statut= '%s', heure_fin= '%s'  WHERE SCP_ID= %s"""% ('No_RSS_found',fin, scp_id))
        db.commit()
        c.close()
        db.close()
        sys.exit(0)   

liste_articles2=list(set(liste_articles2))

# On interroge la base SQL pour savoir si l'url d'un article a déjà été scrappée
liste_articles_scrap=[]
scrapped_already=0

c=db.cursor()
for m in range(len(liste_articles2)) :
    pos=liste_articles2[m].find("www.ft.com")
    if pos <> -1 :
        c.execute("""SELECT ART_ID FROM article_preprocessing WHERE url= ('%s') """%liste_articles2[m])
        if c.fetchone()<> None :
            scrapped_already+=1
        else:
            liste_articles_scrap.append(liste_articles2[m])

### debug ####
#print(str(scrapped_already)+' articles sur un total de ' + str(len(liste_articles2)) + ' ont déjà été scrappés nous les retirons de la liste')
c.close()

# Si la liste des articles ne contient aucun élément nous annulons le scrapping et effaçons l'entrée dans la BD #
if len(liste_articles_scrap) == 0 :
    c=db.cursor()
    c.execute("""SELECT SCP_ID FROM scraping WHERE SCE_ID = (%s) AND DAT_ID = (%s) AND heure_debut = ('%s')  """%(sce_id,dat_id,debut))
    if c.fetchone() <> None:
        c.execute("""DELETE FROM scraping WHERE SCE_ID = (%s) AND DAT_ID = (%s) AND heure_debut = ('%s')  """%(sce_id,dat_id,debut))
        db.commit()
        c.close()
        db.close()
        sys.exit(0)
    
    c.close()

# On crée une fonction qui formate les articles#
def formatage_article(article):
    only_tags_with_Content = SoupStrainer(id="storyContent")
    soup = BeautifulSoup(r.text,parse_only=only_tags_with_Content)
    for div in soup.findAll('div', ['promobox', 'promoboxAlternate', 'storyvideo','storyvideonojs', 'expandable-image','story-package', 'insideArticleShare', 'fullstoryImage', 'fullstoryImageLeft', 'article', 'pullquote', 'pullquoteAlternate' ] ):
        div.extract()
    for q in soup.findAll('q'):
        q.extract()
    for s in soup.findAll('script'):
        s.extract()
    for ns in soup.findAll('noscript'):
        ns.extract()  
    VALID_TAGS = ['div','p']
    for tag in soup.findAll(True):
        if tag.name not in VALID_TAGS:
            tag.replaceWith(tag.renderContents())
    data= soup.encode('utf8')
    data= unidecode(str(data).decode('utf8'))
    data= data.replace("</p>","")
    data= data.replace("\n","")
    data= data.replace("<div id=\"storyContent\">","")
    data= data.replace("</div>","")
    data=data.split("<p>")
    if data[0]=="<!DOCTYPE html>":
        del data[0]
    #on rajoute le titre de l'article en derniere position#
    only_tags_with_title = SoupStrainer('title')
    soup2 = BeautifulSoup(r.text,parse_only=only_tags_with_title)
    title=soup2.encode('utf8')
    title= unidecode(str(title).decode('utf8'))
    title= title.replace("<!DOCTYPE html>","")
    title= title.replace("<title>","")
    title= title.replace("</title>","")
    title= title.replace("\n","")
    data.append(title)
    return data

# On scrappe et formate ensuite une liste de liens d'articles#

##############################################################################
headers = {'user-agent': 'Mozilla/5.0'}
url_login="https://registration.ft.com/registration/barrier/login"

# On rentre nos identifiants pour ouvrir une session#
payload = config_bdf.payload

##############################################################################

# Use 'with' to ensure the session context is closed after use.
with requests.Session() as s:
    erreur=0
    s.post(url_login, headers=headers, data=payload, verify=False, proxies={"https":"https://"+config_bdf.proxy_user+":"+config_bdf.proxy_password+"@"+config_bdf.http_proxy})
    for k in range(len(liste_articles_scrap)):
        url_article=str(liste_articles_scrap[k])
                
        try:
            time.sleep(2)
            r = s.get(liste_articles_scrap[k], proxies={"http":"http://"+config_bdf.proxy_user+":"+config_bdf.proxy_password+"@"+config_bdf.http_proxy})
	    time.sleep(2)
            r.raise_for_status()
            article=formatage_article(r)
            c=db.cursor()
            # On cherche l'ID de l'article dans la BD et on le crée si il n'existe pas, on reprend l'ART_ID si l'article existe déjà#
            c.execute("""SELECT ART_ID FROM article_preprocessing WHERE SCP_ID = (%s) AND url = ('%s')"""%(scp_id,url_article))
            test=c.fetchone()
            if test == None :
                c.execute("""INSERT INTO article_preprocessing (SCP_ID,url,statut) VALUES (%s,'%s','%s')"""%(scp_id,url_article,'WAITING'))
                c.execute("""SELECT ART_ID FROM article_preprocessing WHERE SCP_ID = (%s) AND url = ('%s')"""%(scp_id,url_article))
                art_id = c.fetchone()[0]
            else :
                art_id = test[0]

            #### this is where we should change to get the date ....

            nom_article='FT-'+str(date_scraping)+'SCP_ID-'+str(scp_id)+'ART_ID-'+str(art_id)
            lien_physique=config_bdf.database_path+nom_article  
            
            fileObject = open(lien_physique,'wb') 
            pickle.dump(article,fileObject)   
            fileObject.close()
            
            c.execute("""UPDATE article_preprocessing SET lien= '%s' WHERE ART_ID = (%s) AND url = ('%s')"""%(lien_physique,art_id,url_article))
            db.commit()
            c.close()
        
        except requests.exceptions.HTTPError as e:
            print e
            c.execute("""UPDATE article_preprocessing SET lien= '%s' WHERE ART_ID = (%s) AND url = ('%s')"""% ('FAIL',art_id,url_article))
            c.execute("""UPDATE scraping SET statut= '%s' WHERE SCP_ID= %s"""% ('Incomplete', scp_id))
            db.commit()
            c.close()
            erreur+=1
    
    fin=datetime.datetime.now()
    c=db.cursor()
    if erreur == 0:
        c.execute("""UPDATE scraping SET statut= '%s', heure_fin= '%s'  WHERE SCP_ID= %s"""% ('OK',fin, scp_id))
    else:
        c.execute("""UPDATE scraping SET statut= '%s', heure_fin= '%s'  WHERE SCP_ID= %s"""% ('Incomplete',fin, scp_id))
    db.commit()
    c.close()  
    db.close()

#print article


