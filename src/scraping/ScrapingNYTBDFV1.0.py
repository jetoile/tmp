# coding=utf-8
"""
   Description : Script de Scraping de l'API du New-York Times

   Usage :
    
        >>> python ScrapingNYTBDF.py -d yyyymmdd -f yyyymmdd

   Input : 
       
        -d --deb         Date à partir de laquelle le scraping doit d'éfectuer
        -f --fin         Date à laquelle le scraping doit d'arreter

  Notes :

        Need a MySQL database setup

   Version : V1.1

"""
import os
import json
import datetime
import pickle
import time
import re

from bs4 import BeautifulSoup
import requests

import sqlite3
#import MySQLdb
from unidecode import unidecode

# import config file => a changer pour avoir quelque chose de plus modulaire
import sys
sys.path.append("../")
import config_bdf

from optparse import OptionParser

############## Ajouter une date à la base de données ##############
############ Format date : datetime.date(YYYY,M,J) ################
def ajout_date(c, date=datetime.date.today()):
    c.execute("""SELECT DAT_ID FROM date WHERE date= ('%s') """%date)
    if c.fetchone() == None:
        c.execute("""INSERT INTO date (date) VALUES ('%s')"""%date)
        db.commit()

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def scraper_sce(source_input, date_scraping,c):

   ############### Pour transformer la date provenant de datetime.date(YYYY,M,DD)#####################
   if date_scraping.month in [10, 11, 12]:
      if date_scraping.day in range(10, 32):
         date_NYT_debut = str(date_scraping.year) + str(date_scraping.month) + str(date_scraping.day)
         date_NYT_fin = date_NYT_debut
      else:
         date_NYT_debut = str(date_scraping.year) + str(date_scraping.month) + str(0) + str(date_scraping.day)
         date_NYT_fin = date_NYT_debut
   else:
      if date_scraping.day in range(10, 32):
         date_NYT_debut = str(date_scraping.year) + str(0) + str(date_scraping.month) + str(date_scraping.day)
         date_NYT_fin = date_NYT_debut
      else:
         date_NYT_debut = str(date_scraping.year) + str(0) + str(date_scraping.month) + str(0) + str(date_scraping.day)
         date_NYT_fin = date_NYT_debut
   ##################################################################################################
   try:
      c.execute("""SELECT SCE_ID FROM source WHERE source='%s' """%source_input)
      sce_id = c.fetchone()[0]
   except TypeError:
      raise TypeError(""" La source demandée est incorrecte. """ )
   try:
      c.execute("""SELECT DAT_ID FROM date WHERE date= ('%s') """%date_scraping)
      dat_id = c.fetchone()[0]
   except TypeError:
      raise TypeError(""" La date demandée n'appartient pas à la base SQL. """)
  
   try:
      debut = datetime.datetime.now()
      c.execute("""INSERT INTO scraping (DAT_ID,SCE_ID,statut,heure_debut,heure_fin) VALUES (%s,%s,'%s','%s','%s')"""%(dat_id, sce_id, 'In process', debut, debut))
      
      db.commit()
   except:
      raise ValueError("pas possible ajout")

   try:
      c.execute("""SELECT SCP_ID FROM scraping WHERE SCE_ID = (%s) AND DAT_ID = (%s) AND heure_debut = ('%s')  """ %(sce_id, dat_id, debut))
      
      scp_id = c.fetchone()[0]
   except:
      raise ValueError("problème  la selection de scp_d")
      #### On fait defiler les pages de resultats jusqu'à avoir recuperer tous les resultats du jour
   try:
      fini = False
      i = 0
      while fini == False:
         
         if source_input == 'NYT':
            URL = "http://api.nytimes.com/svc/search/v2/articlesearch.json?begin_date=" + date_NYT_debut + "&end_date=" + date_NYT_fin \
                  + "&fq=source:(%22The%20New%20York%20Times%22)+AND+section_name:(%22Business+Day%22)&page=" + str(
                     i) + "&api-key=" + config_bdf.api_key_NYT
            
         elif source_input == 'Krugman':
            URL = "http://api.nytimes.com/svc/search/v2/articlesearch.json?begin_date=" + date_NYT_debut + "&end_date=" + date_NYT_fin \
                  + "&q=Krugman&page=" + str(i) + "&api-key=" + config_bdf.api_key_NYT
            
         elif source_input == 'Reuters':
            URL = "http://api.nytimes.com/svc/search/v2/articlesearch.json?begin_date=" + date_NYT_debut + "&end_date=" + date_NYT_fin \
                  + "&fq=source:(%22REUTERS%22)+AND+section_name:(%22Business+Day%22)&page=" + str(
                     i) + "&api-key=" + config_bdf.api_key_NYT

         else:
            raise ValueError("La source n'est pas reconnue")

         r = requests.get(URL, proxies={"http":"http://"+config_bdf.proxy_user+":"+config_bdf.proxy_password+"@"+config_bdf.http_proxy})
         #r = requests.get(URL)
         rJSON = json.loads(r.text)
 
         #### Si ERROR ou plus de resultats on s'arrete
         if rJSON['status'] == 'ERROR':
            fini = True
            #print("""STATUS ERROR : L'API affiche le statut ERROR, la clé API a peut-être expiré""")
            fin_error = datetime.datetime.now()
            c.execute("""UPDATE scraping SET statut= '%s', heure_fin= %s  WHERE SCP_ID= %s"""%('KO', fin_error, scp_id))
            db.commit()
            c.close()
            db.close()
            raise ValueError("Le JSON a reçu un statut ERROR")
         elif rJSON['response']['docs'] == []:
            fini = True
         else:
            ###On nomme l'aricle par rapport à son ART_ID
            
            for j, element in enumerate(rJSON["response"]["docs"]):
               
               liste_res = []
               lien_url = element["web_url"]
               if lien_url == None:
                  print("Un lien URL était absent")
                  continue
               elif 'slideshow' in lien_url:
                  print("Certains liens sont des images")
                  continue
               elif '/www.nytimes.com/video' in lien_url:
                  print("Certains liens sont des vidéos")
                  continue
               elif 'www.nytimes.com/interactive' in lien_url:
                  print("Certains liens sont interactifs")
                  continue
               else:
                  with requests.Session() as s:
                     try:
                        r_article = s.get(lien_url, proxies={"http":"http://"+config_bdf.proxy_user+":"+config_bdf.proxy_password+"@"+    config_bdf.http_proxy})
                     except:
                        print(""" Probleme dans get url""")
                        continue

                     if r_article.status_code == 404:
                        print(""" Erreur 404 : un lien de l'API n'existe plus """)
                        try:
                           liste_res = []
                           if element["web_url"] <> None:
                              lien_url = element["web_url"]
                           else:
                              lien_url = "url_manquant"
                           nom_article = source_input + "abstract" + str(date_scraping) + '-' + str(scp_id) + 'page' + str(i + 1) + 'art' + str(j + 1)
                           lien_physique = config_bdf.output_dir + nom_article
                           c.execute("""INSERT INTO article_preprocessing (SCP_ID,url,statut,lien) VALUES (%s,'%s','%s','%s')"""%(scp_id, lien_url, 'WAITING', lien_physique))
                           db.commit()
                           f = open(config_bdf.output_dir+'/'+nom_article, 'wb')
                           snippet = element['snippet']
                           lead_paragraph = element['lead_paragraph']
                           abstract = element['abstract']
                           if snippet <> None:
                              liste_res.append(snippet)
                              
                           elif lead_paragraph <> None and lead_paragraph <> snippet:
                              liste_res.append(lead_paragraph)
                                 
                           elif abstract <> None and abstract <> snippet and abstract <> lead_paragraph:
                              liste_res.append(abstract)
                           pickle.dump(liste_res, f)
                           f.close()
                        except:
                           print("""Probleme dans la récupération de l'abstract""")
                           continue

                     else:

                        try:
                           soup = BeautifulSoup(r_article.text, 'lxml')
                           texte = soup.find_all("p", attrs={"class": ["story-body-text story-content", "story-body-text", "articleBody"]})
                              
                           if texte == []:
                              texte = soup.find_all("p", attrs={"itemprop": "articleBody"})
                           else:
                              pass
                           if texte == []:
                              print("""Un des liens n'a pas pu être parsé correctement""")
                              try:
                                 liste_res = []
                                 if element["web_url"] <> None:
                                    lien_url = element["web_url"]
                                 else:
                                    lien_url = "url_manquant"
                                 nom_article = source_input + "abstract" + str(date_scraping) + '-' + str(scp_id) + 'page' + str(i + 1) + 'art' + str(j + 1)
                                 lien_physique = config_bdf.output_dir + nom_article
                                 c.execute(
                                    """INSERT INTO article_preprocessing (SCP_ID,url,statut,lien) VALUES (%s,'%s','%s','%s')"""% (scp_id, lien_url, 'WAITING', lien_physique))
                                 db.commit()
                                 f = open(config_bdf.output_dir+'/'+nom_article, 'wb')
                                 snippet = element['snippet']
                                 lead_paragraph = element['lead_paragraph']
                                 abstract = element['abstract']
                                 if snippet <> None:
                                    liste_res.append(snippet)
                                          
                                 elif lead_paragraph <> None and lead_paragraph <> snippet:
                                    liste_res.append(lead_paragraph)
                                    
                                 elif abstract <> None and abstract <> snippet and abstract <> lead_paragraph:
                                    liste_res.append(abstract)
                                 pickle.dump(liste_res, f)
                                 f.close()
                              except:
                                 print("""Probleme dans la récupération de l'abstract""")
                                 continue

                           else:
                              nom_article = source_input + str(date_scraping) + '-' + str(scp_id) + 'page' + str(i + 1) + 'art' + str(j + 1)
                              lien_physique = config_bdf.output_dir + nom_article 
                        except:
                           print(""" Probleme dans le parsing beautifulsoup """)
                              
                        c.execute("""SELECT url FROM article_preprocessing """)

                        test = c.fetchall()
                           
                        if lien_url in test:
                           print("""Certains liens avaient déjà été scrapés""")
                           continue
                        else:
                              
                           try:
                              c.execute( """INSERT INTO article_preprocessing (SCP_ID,url,statut,lien) VALUES (%s,'%s','%s','%s')"""%(scp_id, lien_url, 'WAITING', lien_physique))
                           except:
                              print("""Probleme pour la creation des elements de la table article_preprocessing""")
                              continue

                           try:
                              for elem in texte:
                                 liste_res.append(cleanhtml(unidecode(str(elem)).decode('utf8')))
                           except:
                              print("""Probleme dans la transformation des paragraphes""")
                              continue

                           try:
                              f = open(config_bdf.output_dir+'/'+nom_article, 'wb')
                              pickle.dump(liste_res, f)
                              f.close()
                           except:
                              print("""Probleme dans la creation du fichier texte""")
                              continue
                           db.commit()
         i = i + 1

      fin = datetime.datetime.now()
      c.execute("""UPDATE scraping SET statut= '%s', heure_fin= '%s'  WHERE SCP_ID= %s"""% ('OK', fin, scp_id))
      db.commit()
      print(""" Etape du scraping : 'OK' """)

   except:
      c.execute("""UPDATE scraping SET statut= '%s', heure_fin= '%s'  WHERE SCP_ID= %s"""%('KO', datetime.datetime.now(), scp_id))
      db.commit()
      print("""Sans autre message d'erreur, l'erreur est non identifiée""")


def scraper(source_input, date_scraping,c):
   
   try:
      c.execute("""SELECT SCE_ID FROM source WHERE source = ('%s') """%source_input)
      sce_id = c.fetchone()[0]
   except TypeError:
      print(""" La source est incorrecte, vérifier l'orthographe.  """)
      raise

   try:
      c.execute("""SELECT DAT_ID FROM date WHERE date= ('%s') """%date_scraping)
   except:
      print('Probleme select date_scraping')
      raise
   try:
      dat_id = c.fetchone()[0]
   except TypeError:
      print(""" Cette date n'existe pas dans la base de données""")
      raise
   except:
      print('Autre erreur avec la date')
      raise

   #try:
   c.execute("""SELECT * FROM scraping WHERE SCE_ID = (%s) AND statut = '%s' AND DAT_ID = %s """%(sce_id, 'OK', dat_id))
   #except:
   #        raise ValueError("probleme dans l'execution de la Selection")
   #try:
   if not c.fetchone():
      scraper_sce(source_input, date_scraping,c)
   #except:
   #   raise ValueError("Probleme dans la definition de la fonction scraper")



def creation_liste_date(start_date, end_date):
    day_count = (end_date - start_date).days + 1
    return [datetime.date(single_date.year, single_date.month, single_date.day) for single_date in
            (start_date + datetime.timedelta(n) for n in range(day_count))]


if __name__ == '__main__':
   
   
   
   ########### Get MySQL DB connection ###########
   mysql_config = config_bdf.mysql
   #db = MySQLdb.connect(host=mysql_config['host'], user=mysql_config['user'], passwd=mysql_config['passwd'], db=mysql_config['db'])
   ############ If we get sqlite db ##############
   db = sqlite3.connect(mysql_config['sqlite_db'])
   c = db.cursor()


   ############ Get date delat ###########
   
   ### get options date 
   parser = OptionParser()
   parser.add_option("-d", "--deb", dest="date_deb", help="date de type yyyymmdd", default="20140908")
   parser.add_option("-f", "--fin", dest="date_fin", help="date de type yyyymmdd", default="20140909")
   (options, args) = parser.parse_args()
   date_deb = options.date_deb
   date_fin = options.date_fin

   try:
      start_date = datetime.date(int(date_deb[0:4]), int(date_deb[4:6]), int(date_deb[6:8]))
      end_date = datetime.date(int(date_fin[0:4]), int(date_fin[4:6]), int(date_fin[6:8]))
   except:
      raise ValueError("Les dates fournis ne correspondent pas à un format de date accepté (yyyymmdd exemple: 20140926")


   liste_date = creation_liste_date(start_date, end_date)

   ############### ligne de code à mettre dans le programme général de Fidel
   for single_date in liste_date:
      ajout_date(c,single_date)
      for source in ['NYT', 'Reuters', 'Krugman']:
         date_scraping = single_date
         source_input = source
         scraper(source_input, date_scraping,c)

   c.close()
   db.close()
