# coding=utf-8
#########################################

# Description :

# L’indice FEARS fabriqué ici est une adaptation de l’indice FEARS présenté dans ce papier de recherche : http://www3.nd.edu/~zda/FEARS.pdf
# La différence majeure est que nous utilisons ici des comptes de mots d’articles de quotidiens comme base de départ plutôt que les valeurs de Google Trends associées à des mots clés (un des avantages est que les articles du jour sont disponibles le jour même alors que les données Google ne le sont que 48h après (de mémoire)).
# Le FEARS ACP est une simple réduction de la dimension en ne gardant que la composante principale du compte des mots clefs.


# USAGE :

# >>>  python FEARS -d yyyymmdd -f yyyymmdd

# Input : 
       
#         -d --deb         Date à partir de laquelle le scraping doit d'éfectuer
#         -f --fin         Date à laquelle le scraping doit d'arreter

# output :

# Les indicateurs FEARS et FEARS ACP sont représentés par des times series en baton

##############################################
import datetime
from datetime import date

import numpy as np
from sklearn import preprocessing
from sklearn.decomposition import PCA

#import MySQLdb
import sqlite3

import sys
sys.path.append("../")
import config_bdf

from optparse import OptionParser

def creation_liste_date(start_date, end_date):
    """Crée une liste des dates intermédiaires"""
    day_count = (end_date - start_date).days + 1
    return [datetime.date(single_date.year, single_date.month, single_date.day) for single_date in (start_date + datetime.timedelta(n) for n in range(day_count))]


def reduce_mot(liste, mot, date):
    """Somme les valeurs par mot et par date"""
    return sum([elem[2] for elem in liste if ( elem[0] == mot and elem[1] == date )])


# #### Création de l'indicatrice des jours de la semaine
def fun_dummy_day(date_1, date_2):
    """Crée la matrice indicatrice par jour des dates intermédiaires"""
    liste_jour = []
    for i in range(1, 8):
        tamp = []
        for elem in creation_liste_date(date_1, date_2):
            if elem.isoweekday() == i:
                r = 1
            else:
                r = 0
            tamp.append(r)
        liste_jour.append(tamp)
    # noinspection PyCallingNonCallable
    return np.matrix(liste_jour).T


def get_counts_by_keyword_and_date_from_sql(start_date, end_date, liste_mots_clefs, liste_source):
    """Se connecte à la BDD et renvoie le compte des mots clés par date, compte et source"""
    mysql = config_bdf.mysql
    #db = MySQLdb.connect(host=mysql['host'], user=mysql['user'], db=mysql['db'])
    db = sqlite3.connect(mysql['sqlite_db'])
    c = db.cursor()
    slk = "('"+"','".join(liste_mots_clefs)+"')"
    sls = "('"+"','".join(liste_source)+"')"
    c.execute(""" SELECT k.keyword, d.date, w.compte, sce.source
            FROM keyword_count w
            INNER JOIN article_preprocessing a ON w.ART_ID=a.ART_ID
            INNER JOIN scraping scp ON a.SCP_ID=scp.SCP_ID
            INNER JOIN keywords k ON k.KEY_ID=w.KEY_ID 
            INNER JOIN date d ON d.DAT_ID=scp.DAT_ID  
            INNER JOIN source sce ON sce.SCE_ID=scp.SCE_ID
            WHERE w.compte<>0 AND d.date >= ('%s') AND d.date <= ('%s')  AND k.keyword IN %s AND sce.source IN %s
            ORDER BY w.compte """%(start_date, end_date, slk, sls))
    res = c.fetchall()
    c.close()
    db.close()
    return res


def get_matrice_fears(start_date, end_date, liste_mots_clefs, liste_source):
    res = get_counts_by_keyword_and_date_from_sql(start_date, end_date, liste_mots_clefs, liste_source)
    liste_date = creation_liste_date(start_date, end_date)
    matrice_fears = np.zeros((len(liste_mots_clefs), len(liste_date)))
    dico_conversion_index = dict([(e, i) for i, e in enumerate(liste_mots_clefs)])
    date_conversion = dict([(e, i) for i, e in enumerate(liste_date)])
    for res_requete in res:
       idc =  datetime.datetime.strptime(res_requete[1], '%Y-%m-%d').date()
       matrice_fears[dico_conversion_index[res_requete[0]], date_conversion[idc]] = matrice_fears[dico_conversion_index[res_requete[0]], date_conversion[idc]] + res_requete[2]
    # noinspection PyCallingNonCallable
    return np.matrix(matrice_fears)


def get_diff_fears(matrice_fears):
    """Renvoie la croissance de fears d'un jour à l'autre, par mot"""
    log_fears = np.log((matrice_fears + 0.01) / (matrice_fears + 0.01).max(1))
    return log_fears[:, 1:] - log_fears[:, 0:-1]


def get_residuals(matrice_fears, start_date, end_date):
    """Renvoie les résidus"""
    indicatrice_jour = fun_dummy_day(start_date, end_date)
    diff_fears = get_diff_fears(matrice_fears)
    X = indicatrice_jour[1:, :]
    residuals = np.zeros(shape=(diff_fears.shape[0], X.shape[0]))
    for i in range(diff_fears.shape[0]):
        y = diff_fears[i, :]
        # mod = argmin ||X mod = y.T ||
        mod, _, _, _ = np.linalg.lstsq(X, y.T)
        res_inter = [elem for (elem,) in (y.T - X * mod).tolist()]
        res_inter_scaled = preprocessing.scale(np.array(res_inter))
        residuals[i] = res_inter_scaled
    return residuals


def fonction_fears(start_date, end_date, liste_mots_clefs, liste_source):
    export_fears, export_ACP, export_global = fonction_fears_(start_date, end_date, liste_mots_clefs, liste_source)
    return str(export_fears), str(export_ACP), str(export_global)


# noinspection PyCallingNonCallable
def fonction_fears_(start_date, end_date, liste_mots_clefs, liste_source):
    liste_date = creation_liste_date(start_date, end_date)
    matrice_fears = get_matrice_fears(start_date, end_date, liste_mots_clefs, liste_source)

    # ###Creation de l'exportation global
    export_global = []
    for j in range(matrice_fears.shape[1]):
        export_global.append(dict([('date', str(liste_date[j]))] +
                                  [(liste_mots_clefs[i], matrice_fears[i, j]) for i in range(matrice_fears.shape[0])]))

    ###############  Commenter/decommenter la ligne ci-dessous pour normaliser ou non les valeurs de fears acp
    matrice_fears = (matrice_fears + 0.01) / (matrice_fears + 0.01).max(1)

    residuals = get_residuals(matrice_fears, start_date, end_date)
    # noinspection PyCallingNonCallable
    SUM_FEARS_SCALED = preprocessing.scale(np.asarray(np.matrix(residuals).sum(0)).reshape(-1))
    export_fears = [{"date": str(date), "value": val} for date, val in zip(liste_date[1:], SUM_FEARS_SCALED)]

    FEARS_ACP = PCA(n_components=1).fit_transform(matrice_fears.T)
    FEARS_ACP_scaled = preprocessing.scale(FEARS_ACP.reshape(-1))
    export_ACP = [{"date": str(date), "value": val} for date, val in zip(liste_date[1:], FEARS_ACP_scaled)]
    for i, elem in enumerate(export_global[1:]):
        elem['FEARS'] = export_fears[i]['value']
        elem['FEARS_ACP'] = export_ACP[i]['value']

    return export_fears, export_ACP, export_global

if __name__ == "__main__":

   ### get options date 
   parser = OptionParser()
   parser.add_option("-d", "--deb", dest="date_deb", help="date de type yyyymmdd", default="20140908")
   parser.add_option("-f", "--fin", dest="date_fin", help="date de type yyyymmdd", default="20140909")
   (options, args) = parser.parse_args()
   date_deb = options.date_deb
   date_fin = options.date_fin

   try:
      start_date = datetime.datetime(int(date_deb[0:4]), int(date_deb[4:6]), int(date_deb[6:8]))
      end_date = datetime.datetime(int(date_fin[0:4]), int(date_fin[4:6]), int(date_fin[6:8]))
   except:
      raise ValueError("Les dates fournis ne correspondent pas à un format de date accepté (yyyymmdd exemple: 20140926")

   liste_source = ["NYT", "FT", "Krugman", "Reuters"]

   liste_mots_clefs = sorted(['recession', 'depression', 'deflation', 'price of gold', 'gold value', 'bankruptcy', 'crisis', 'gdp', 'charity', 'unemployment', 'inflation rate', 'short sale', 'foreclosure', 'capitalization', 'poverty', 'volatility', 'resolution', 'sovereign', 'deficit', 'debt', 'leverage', 'failure', 'default', 'exposure', 'credit', 'bubble', 'boom', 'bust', 'krach', 'risk', 'liquidity', 'funding', 'interconnectness', 'contagion', 'domino', 'CDS', 'social security card', 'stock market', 'VIX index', 'bail-in', 'bail-out', 'domino effect', 'systemic risk', 'market stress', 'stress test'])

   export_fears, export_ACP, export_global = fonction_fears(start_date.date(), end_date.date(), liste_mots_clefs, liste_source)
