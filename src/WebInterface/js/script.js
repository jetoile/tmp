/**
 * Created by PC on 28/08/2014.
 */
$(function(){
    $("#home").load("html/home.html");
    //$("#btnCsvExport").attr("href","http://veille-eco.bluestone.fr:"+window.location.port+"/file.csv");
	 $("#btnCsvExport").attr("href","http://lxdv291a.unix-int.intra-int.bdf-int.local:"+window.location.port+"/file.csv");
	var button = "";
    $("#exportCsvForm").submit(function(event){
        event.preventDefault();
        var $this = $(this);
        $.ajax({
            url: $this.attr('action'), // Le nom du fichier indiqué dans le formulaire
            type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
            data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            success:function(data){}
        })
    });
	$("#home").delegate("#btnFears, #btnWordCloud","click",function(event){
		button = event.target.value;
		$("#visualization").val(button);
	});
	$("#home").delegate("a.select","click",function(event){
        if($(this).text() == 'Select all'){
            $("input[name='word']").prop("checked",true);
        }else{
            $("input[name='word']").prop("checked",false);
        }

	});
    $("#home").delegate("a.select-source","click",function(event){
        if($(this).text() == 'Select all'){
            $("input[name='source']").prop("checked",true);
        }else{
            $("input[name='source']").prop("checked",false);
        }
    })
	$("#home").delegate(".date, input[name='source'], input[name='word']","focus",function(event){
        $("#message-sources-required").empty();
        $("#message-words-required").empty();
        $("#message-dates-sorted").empty();
		$(".date").datepicker();
	});

    function parseDate(strDate){
        var strs = strDate.split('/');
        var date = new Date(parseInt(strs[2],10),parseInt(strs[0],10),parseInt(strs[1],10))
        return date;
    }
	
    $("#home").delegate("#form","submit",function(e){
        e.preventDefault();
        var $this = $(this); // L'objet jQuery du formulaire
		console.log($this.serialize());
		//validation du formulaire
        var n = $("input[name='source']:checked").length;
        if(n===0){
            $("#message-sources-required").html("<span class='red'>Au moins une des sources est requise</span>");
            return;
        }
        n = $("input[name='word']:checked").length;
        if(n==0){
            $("#message-words-required").html("<span class='red'>Au moins un mot clef est requis</span>");
            return;
        }
        if(parseDate($("#endDate").val()) < parseDate($("#startDate").val())){
            $("#message-dates-sorted").html("<span class='red'>La date de d&#233but doit etre inf&#233rieure a la date de fin</span>");
            return;
        }
       $("#wait").show();
       $.ajax({
            url: $this.attr('action'), // Le nom du fichier indiqué dans le formulaire
            type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
            data: $this.serialize(), // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
            success: function(data) {
                var startTime = new Date().getTime();
                while (new Date().getTime() < startTime + 2000);
                $("#wait").hide();
				if(button === 'fears'){
					$("#barChart").empty();
					var json = JSON.parse(data);
					drawBarChart(json.fears,"FEARS");
					drawBarChart(json.fearsACP,"FEARS_ACP");
				} else if(button === 'concept_analysis'){
					$("#wordCloudChart").empty();
					drawWordCloud('/data/cloud2014-01-01to2014-01-31exemple');
                    $("#linesChart").empty();
                    drawMultilines('/data/multi_ligne_exemple5');
				}else{
				}
				
                
            },
			error:function(rq,textStatus, error){
				$("#wait").hide();
				alert(textStatus);
			}
        });
    });
});
function multiply(d){
	d.size = (d.size.toFixed(2)*500).toFixed(0);
    return d;
}
function drawWordCloud(filename){
    var tip = d3.tip()
        .offset([-10, 0])
        .html(function(d) {
            return "<div class='position-tooltip'><table class='c3-tooltip' id='tabTooltip'><tbody><tr bgcolor='"+d.color+"'><th colspan='2' style='background-color:"+ d.color+"'>"+d.title+"</th></tr><tr><td>Probabilit&#233 cluster</td><td class='value'>"+(d.proba_cluster.toFixed(2)*100).toFixed(0)+" %</td></tr><tr><td>Nombre d&#145articles</td><td class='value'>"+d.nb_article+"</td></tr><tr><td>Nombre d&#145apparitions</td><td class='value'>"+d.nb_apparition+"</td></tr></tbody></table></div>";
        });
		
    d3.json(filename,function (data){
        d3.layout.cloud().size([900,600])
            .words(data.map(multiply))
            .font("Impact")
            .rotate(0)
            .fontSize(function(d) { return d.size; })
            .on("end", draw)
            .start();

        function draw(words){
            var svg = d3.select("#wordCloudChart").append("svg");
            svg.call(tip);
            svg.attr("width", 900)
                .attr("height", 600)
                .append("g")
                .attr("transform", "translate(450,300)")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function(d,i) { return d.size + "px"; })
                .style("font-family", "Impact")
                .style("fill", function(d,i) { return d.color})
                .attr("text-anchor", "middle")
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
				.attr("class",function(d){return d.color})
                .on('mouseover', function(d){return tip.show(d)})
                .on('mouseout', function(d){return tip.hide(d)})
                .text(function(d,i) { return d.text; });
        }
    });

}

function drawBarChart(data,chartTitle){
	var tip = d3.tip()
	  .offset([-10, 0])
	  .html(function(d) {
		return "<table class='c3-tooltip'><tbody><tr><th colspan='2'>"+d.date+"</th></tr><tr><td>value</td><td class='value'>"+d.value.toFixed(2)+"</td></tr></tbody></table>";
	  });
	var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1080 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

	var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
		.range([0,height]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left")
		//.ticks(10, "%");

	var svg = d3.select("#barChart").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
	  .append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	svg.call(tip);

	  var fearsData = data.map(function(d,i){ 
		d.i = i;
		return d;
	  });
	  x.domain(fearsData.map(function(d) { return d.i; }));
	  y.domain([d3.max(fearsData,function(d){return d.value}),d3.min(fearsData,function(d){return d.value})]).nice();

	  var xAxisElements = svg.append("g")
		  .attr("class", "x axis")
		  .attr("transform", "translate(0," + y(0) + ")")
		  .call(xAxis);
	  xAxisElements.selectAll("text").remove();
	  xAxisElements.selectAll("line").remove();

	  svg.append("g")
		  .attr("class", "y axis")
		  .call(yAxis)
		.append("text")
		  .attr("transform", "rotate(-90)")
		  .attr("y", 6)
		  .attr("dy", ".71em")
		  .style("text-anchor", "end")
		  .text("Fears");

	  svg.selectAll(".bar")
		  .data(fearsData)
		.enter().append("rect")
		  .attr("class", "bar")
		  .attr("x", function(d) { return (x(d.i)+x.rangeBand()/2)-10; })
		  .attr("width", 2)
		  .attr("y", function(d) { return y(Math.max(0,d.value)); })
		  .attr("height", function(d) { return Math.abs(y(0)-y(d.value)); })
		  .on("mouseover",tip.show)
		  .on("mouseout",tip.hide);
		  
		  svg.append("g")
		  .attr("class", "x axis");
}

function drawMultilines(filename){
    var tabColor = ["turquoise","brown","grey","green","red","blue","orange","purple","black","violet"];
    var xAxis = ['x'];
    d3.json(filename,function(data){
        var titles = data.pop();
        var values0 = [titles.title0];
        var values1 = [titles.title1];
        var values2 = [titles.title2];
        var values3 = [titles.title3];
        var values4 = [titles.title4];
        var values5 = [titles.title5];
        var values6 = [titles.title6];
        var values7 = [titles.title7];
        var values8 = [titles.title8];
        var values9 = [titles.title9];

        data.forEach(function (d){
            values0.push(d.value0.toFixed(0));
            values1.push(d.value1.toFixed(0));
            values2.push(d.value2.toFixed(0));
            values3.push(d.value3.toFixed(0));
            values4.push(d.value4.toFixed(0));
            values5.push(d.value5.toFixed(0));
            values6.push(d.value6.toFixed(0));
            values7.push(d.value7.toFixed(0));
            values8.push(d.value8.toFixed(0));
            values9.push(d.value9.toFixed(0));
            xAxis.push(d.date);
        });

        var chart = c3.generate({
            bindto: '#linesChart',
            data: {
                x:'x',
                columns: [xAxis,values0,values1,values2,values3,values4,values5,values6,values7,values8,values9]
            },
            tooltip:{
                title: function(d){return d}
            },
            axis:{
                x:{
                    type:'timeseries',
                    tick:{
                        format:'%Y-%m-%d'
                    }
                }
            },
            color:{
                pattern:tabColor
            }
        });
    });
}
