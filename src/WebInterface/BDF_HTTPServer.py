from BaseHTTPServer import BaseHTTPRequestHandler
from SimpleHTTPServer import SimpleHTTPRequestHandler
import cgi
import datetime
import csv
import json
import os
import sys
#from collections import OrderedDict

import sys
sys.path.append("../FEARS/")
sys.path.append("../")
from FEARS import *
import config_bdf

class GetHandler(SimpleHTTPRequestHandler):

    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile, 
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })
        keywords = sorted(form.getlist('word'))
        sources = form.getlist('source')
        button = form.getvalue('visualization')
        startDateStr = form.getvalue('startDate')
        endDateStr = form.getvalue('endDate') 
        self.send_response(200)    
        if button  == "concept_analysis":
            self.end_headers()
            self.wfile.write(config_bdf.concept_analysis_path)
        elif button == "fears":
            #self.wfile.write("Content-Type:application/json\r\n")
            self.end_headers()
            if os.path.isfile(config_bdf.csv_file_created_web_server):
                os.remove(config_bdf.csv_file_created_web_server)
            startDate = datetime.datetime.strptime(startDateStr,'%m/%d/%Y').date()
            endDate = datetime.datetime.strptime(endDateStr,'%m/%d/%Y').date()
            sortie = fonction_fears(startDate,endDate,keywords,sources)
            jsonStr = '{"fears":'+sortie[0].replace("'","\"")+', "fearsACP":'+sortie[1].replace("'","\"")+', "csvfile":'+sortie[2].replace("'","\"")+'}'
            x = json.loads(sortie[2].replace("'","\""))
            with open(config_bdf.csv_file_created_web_server,'wb') as f:
                headers = x[1].keys()
                dic_writer = csv.DictWriter(f,fieldnames=headers,delimiter=";")
#                dic_writer.writeheader()
                dic_writer.writerows(x)
            self.wfile.write(jsonStr)
        else :
            print(button)
        
        
        return


if __name__ == '__main__':
    from BaseHTTPServer import HTTPServer
    server = HTTPServer((config_bdf.server_address, int(sys.argv[1])), GetHandler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()
