# coding=utf-8

#########################################################


# Description : 

# Le preprocessing est commun à toutes les sources, il transforme chaque texte (après épuration et lemmatisation) en plusieurs dictionnaires python (un par paragraphe) sans effacer le texte récupéré lors du scraping (ceci permet notamment de pouvoir rechercher des nouveaux mots clés composés dans le texte afin de remplir rétrospectivement la base en cas d’ajout différé de mots clés)

# Usage :

#  >>>python preprocessigBDF_base1.py

# Dependance :

# - Base de données MySQL

# - Libraie treetaggerwrapper


#############################################################

import os
import pickle
import logging
#from collections import Counter

#import MySQLdb
import sqlite3

from treetaggerwrapper import TreeTagger
import sys
sys.path.append("../")
import config_bdf

common_words = ['<unknown>', '@card@', '@', ',', '@ord@', '.', ';', '!', '?', '<', '>', "'", '(', ')', ':', '/', '-',
                'text', 'time', 'person', 'year', 'way', 'day', 'thing', 'man', 'world', 'life', 'hand', 'part',
                'child', 'eye', 'woman', 'place', 'work', 'week', 'case', 'point', 'company', 'number', 'group',
                'problem', 'fact', 'be', 'have', 'do', 'say', 'get', 'make', 'go', 'know', 'take', 'see', 'come',
                'think', 'look', 'want', 'give', 'use', 'find', 'tell', 'ask', 'work', 'seem', 'feel', 'try', 'leave',
                'call', 'good', 'new', 'first', 'last', 'long', 'great', 'little', 'own', 'other', 'old', 'right',
                'big', 'high', 'different', 'small', 'large', 'next', 'early', 'young', 'important', 'few',
                'public''bad', 'same', 'able', 'which', 'to', 'of', 'in', 'for', 'on', 'with', 'at', 'by', 'from', 'up',
                'about', 'into', 'over', 'after', 'beneath', 'under', 'above', '\"', '=', "'s", 'its', '&', 'e', 'than',
                'the', 'and', 'a', 'that', 'I', 'it', 'not', 'he', 'as', 'you', 'this', 'but', 'his', 'they', 'her',
                'she', 'or', 'an', 'will', 'my', 'one', 'all', 'would', 'there', 'their', 'per', 'cent', 'Mr', 'more',
                'thereby', 'might', 'also', 'keep', 'some', 'such', 'since', 'while', 'most', 'when', 'include', 'who',
                'so', '{', '}''1', '2', '3', '4', '5', '6', '7', '8', '9', '...', 'because', 'what', 'just', 'if',
                'may', 'could', 'yet', 'these', 'our', 'only', 'yes', 'no', 'whose', 'during', 'however', 'we', 'many',
                'already', 'always', 'those', 'even', 'almost', 'The', '*', 'A', '|']
common_words = set(common_words)



os.environ['TAGDIR'] = config_bdf.treetagger_path

mysql = config_bdf.mysql
#db = MySQLdb.connect(host=mysql['host'], user=mysql['user'], db=mysql['db'])
db = sqlite3.connect(mysql['sqlite_db'])

def tagging(line):
    tagger = TreeTagger(TAGOPT='-token -lemma', TAGLANG='en')
    return tagger.TagText(line)


# fonction qui recupere uniqument le lemme
def recup_mot(mot):
    num = mot.rfind('\t')
    return mot[num + 1:]


def filtre_mot(mot):
    if mot not in common_words:
        return mot
    else:
        pass


# ############## Fonction de preprocessing qui ne prend aucun paramètre
def preprocessing():
   # ##### Recuperation des mots clefs doubles
        
   c = db.cursor()
   
   c.execute(""" SELECT keyword FROM keywords """)
   set_key_word = c.fetchall()
   keywords = [elem[0] for elem in set_key_word if ' ' in elem[0]]
   all_keywords = [elem[0] for elem in set_key_word]
   ############################################################
   
   ####### Recuperation des art_id et des liens physiques en attente de preprocessing
   
   c.execute(""" SELECT ART_ID,lien FROM article_preprocessing where statut='%s' """%('WAITING'))
   
   artID_lien = c.fetchall()
   
   if artID_lien == ():
      print("""Aucun article n'est en attente de preprocessing""")
      c.close()
      db.close()
      raise

   else:
      c.close()

   for art_id, lien_phys in artID_lien:
         
      ############## Transformation du fichier texte en dictionnaires
      try:
         read_file = open(lien_phys, 'rb')
      except:
         print('Il y a eu un problème avec un lien physique')
         continue

      try:
         texte = pickle.load(read_file)
      except EOFError:
         print('Il y a eu quelques problèmes avec le pickle')
         continue
      except:
         print(logging.exception('traceback error'))
         print('Probleme dans le pickle.load')
         continue
      read_file.close()

      try:
         num = lien_phys.rfind('/')
         write_file = open(config_bdf.preprocessed_data_path + lien_phys[num + 1:], 'wb')
      except:
         print(""" Probleme dans l'ouverture du fichier en mode wb """)
         continue

      dico_compte = dict([(elem, 0) for elem in all_keywords])
      
      try:
         res_final = []
            
         for paragraphe in texte:
            if paragraphe <> str():
               for element in all_keywords:
                  incr = paragraphe.count(element)
                  dico_compte[element] = dico_compte[element] + incr

               mots_doubles = []
                  
               for my_string in keywords:
                  incr = paragraphe.count(my_string)
                  mots_doubles = mots_doubles + ([my_string] * incr) 
               ############## wordcount des mots clefs doubles
                                

               word_dict = [filtre_mot(recup_mot(elem)) for elem in tagging(paragraphe) if filtre_mot(recup_mot(elem)) <> None] +     mots_doubles
               res_final.append(word_dict)

            else:
               continue

      except:
         print(""" Probleme dans la transformation du texte """)
         # print(logging.exception('traceback error'))
         continue

      try:
         pickle.dump(res_final, write_file)
         write_file.close()
      except:
         print(""" Problème dans l'écriture des fichiers""")
         continue


      #####################################################################
      
      ##################    Changement du statut dans article_preprocessing
      c = db.cursor()
      
      for key, val in dico_compte.items():
         try:
            c.execute(""" SELECT KEY_ID FROM keywords WHERE keyword='%s' """%key)
            key_id = c.fetchone()[0]
         except TypeError:
            print('Il y un problème de mot-clé')
            c.close()
            db.close()
            raise
         try:
            c.execute("""INSERT INTO keyword_count (ART_ID,KEY_ID,compte) VALUES (%s,%s,'%s')"""%(art_id, key_id, val))
         except:
            print('Il y a eu un problème lors du remplissage de la table keyword_count')
            c.close()
            db.close()
            raise

      c.execute("""UPDATE article_preprocessing SET statut= '%s' WHERE ART_ID= %s"""%('DONE', art_id))
      c.execute("""UPDATE article_preprocessing SET lien_prep= '%s' WHERE ART_ID= %s"""%(config_bdf.preprocessed_data_path+ lien_phys[num + 1:], art_id))
      db.commit()
         
      c.close()

   #except:
   #     raise ValueError("Le travail ne s'est ps passé correctement, 'enfant est difforme")


######################## debug #########################

# c = db.cursor()

# c.execute(""" SELECT keyword FROM keywords """)
# set_key_word = c.fetchall()
# keywords = [elem[0] for elem in set_key_word if ' ' in elem[0]]
# all_keywords = [elem[0] for elem in set_key_word]
# c.close()

# lien_phys= config_bdf.lien_phys

# try:
#     read_file = open(lien_phys, 'rb')
# except:
#     print('Il y a eu un problème avec un lien physique')

# try:
#     texte = pickle.load(read_file)
# except EOFError:
#     print('Il y a eu quelques problèmes avec le pickle')
#     raise
# except:
#     print(logging.exception('traceback error'))
#     raise IOError('Probleme dans le pickle.load')

# read_file.close()
# try:
#     num = lien_phys.rfind('/')
#     write_file = open(config_bdf.preprocessed_data_path + lien_phys[num + 1:], 'wb')
# except:
#    raise IOError(""" Probleme dans l'ouverture du fichier en mode wb """)

# dico_compte = dict([(elem, 0) for elem in all_keywords])

# res_final = []

# for paragraphe in texte:
#     if paragraphe != str():
#         for element in all_keywords:
#             incr = paragraphe.count(element)
#             dico_compte[element] = dico_compte[element] + incr

#         mots_doubles = []

#         for my_string in keywords:
#             incr = paragraphe.count(my_string)
#             mots_doubles = mots_doubles + ([my_string] * incr)  #

#         word_dict = Counter([filtre_mot(recup_mot(elem)) for elem in tagging(paragraphe) if
#                              filtre_mot(recup_mot(elem)) <> None] + mots_doubles)
#         res_final.append(word_dict)

#     else:
#         print('pbm')

if __name__ == "__main__":
    preprocessing()
