set -e
set -x

# capture stdin et stderr to a file
exec > >(tee logfile.txt)
exec 2>&1

while true; do date; sleep 60; done &
pid=$!

echo generating the mallet input
#time ../mallet-2.0.7/bin/mallet svmlight --input data.svm \
time ../mallet-2.0.7/bin/mallet import-dir --input data \
    --keep-sequence --remove-stopwords TRUE \
    --output data.mallet

date
echo fitting the model
time ../mallet-2.0.7/bin/mallet train-topics \
    --num-topics 100 \
    --num-threads 7 \
    --random-seed 1 \
    --optimize-interval 20 \
    --num-iterations 100 \
    --output-model-interval 200 \
    --output-state-interval 200 \
    --input ./data.mallet \
    --output-state ./state.gz \
    --output-model ./model \
    --output-doc-topics ./doc_topics \
    --output-topic-keys ./topic_keys \
    --topic-word-weights-file word-weights-file \
    --word-topic-counts-file ./word-topic-counts-file \
    --xml-topic-report xml-topic-report \
    --xml-topic-phrase-report xml-topic-phrase-report 

kill $pid
