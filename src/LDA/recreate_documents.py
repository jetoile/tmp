# coding=utf-8

import cPickle
import multiprocessing
from operator import add
#from collections import Counter
from Counter import Counter
import traceback
import os
import re
import shutil

import unidecode

__author__ = 'mbz'

import sys
sys.path.append("../")
import config_bdf


def read_article(path):
   f = open(path)
   s = f.read()
   # serialize les mots clefs
   try:
      pickled = cPickle.loads(s)
   except:
      print("could not load file :" + path)
      return None
   # met à plat les listes
   pickled = [item for sublist in pickled for item in sublist]
   
   # ligne remplacé 
   #counter = reduce(add, pickled, Counter())
   # en : 
   # creation d'un counter (ici le counter expliquer ci avant)
   counter = Counter()
   # je prend ma liste 
   for p in pickled:
      # et j'augmente le counter du mot
      counter[p]+=1
   return counter


def create_line(counter, svm_format=False):
    if not isinstance(counter, Counter):
        return ""
    if not all(isinstance(w, unicode) for w in counter):
        new_counter = Counter()
        for w, n in counter.items():
            try:
                w_unicode = unicode(w)
            except Exception as e:
                pass
            else:
                new_counter[w] = n
        counter = new_counter
    if svm_format:
        try:
            return u" ".join(u"{}:{}".format(w.replace(u" ", u"_"), n_occ) for (w, n_occ) in counter.items())
        except UnicodeError:
            try:
                return " ".join("{}:{}".format(w.replace(u" ", u"_"), n_occ) for (w, n_occ) in counter.items())
            except UnicodeError:
                return " ".join("{}:{}".format(unidecode.unidecode(w).replace(u" ", u"_"), n_occ)
                                for (w, n_occ) in counter.items())
    else:
        return u" ".join(u" ".join(w for _ in xrange(n_occ)) for (w, n_occ) in counter.items())



def get_paths(directory=config_bdf.preprocessed_data_path):
    return [os.path.join(directory, f) for f in os.listdir(directory)]


def process_article(path, dest_dir=config_bdf.recreated_doc_path ):
   re_date = re.compile(r".*([0-9]{4}-[0-9]{2}-[0-9]{2}).*")
   new_name = re_date.match(path).groups()[0] + "_" + os.path.basename(path)
   c_article = read_article(path)
   if c_article:
      line = create_line(c_article)
      with open(os.path.join(dest_dir, new_name), "w") as f:
         f.write(line)



#if __name__ == "__main__":
#    try:
#        shutil.rmtree(config_bdf.recreated_doc_path)
#    except:
#        pass
#    os.mkdir(config_bdf.recreated_doc_path)
#    paths = get_paths()
#    pool = multiprocessing.Pool(7)
#    pool.map(process_article, paths)

if __name__ == "__main__":
   try:
      shutil.rmtree(config_bdf.recreated_doc_path)
   except:
      pass
   os.mkdir(config_bdf.recreated_doc_path)
   paths = get_paths()
   #pool = multiprocessing.Pool(7)
   #pool.map(process_article, paths)
   for path in paths:
      process_article(path)

