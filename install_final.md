#Installation des prérequis

##Prérequis

Utilisation de python 2.6.6

##Méthode 1

La majorité des libs nécessaires se trouvent dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/lib`.

Il faut, de plus installer les librairies systèmes :

Sur lxdv291a, un problème de configuration empêche l'installation de package. Pour y remédier, faire :

```
mv /etc/yum.repos.d/HDP-UTILS.repo /etc/yum.repos.d/HDP-UTILS.repo.bak
yum update
```

Installation des dépendances système utiles :

```
yum install libyaml libxslt-devel libxml2-devel gcc 
yum install gcc-c++ cmake
yum install lapack blas
```

Installer les packages systèmes suivants : 

```
wget ftp://195.220.108.108/linux/centos/6.6/os/x86_64/Packages/lapack-devel-3.2.1-4.el6.x86_64.rpm
wget ftp://195.220.108.108/linux/centos/6.6/os/x86_64/Packages/blas-devel-3.2.1-4.el6.x86_64.rpm

yum install blas-devel-3.2.1-4.el6.x86_64.rpm
yum install lapack-devel-3.2.1-4.el6.x86_64.rpm
```


Installation des dépendances python supplémentaire (installation nécessaire car sinon il manque des fichiers de configuration) :

Décompresser /home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/numpy-1.9.2.tar.gz
```
cd /home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/
tar xzvf numpy-1.9.2.tar.gz

cd numpy-1.9.2
python setup.py install 
```

##Méthode 2

Cette méthode recompile tous les packages python nécessaires.

###Packages systèmes

Sur lxdv291a, un problème de configuration empêche l'installation de package. Pour y remédier, faire :

```
mv /etc/yum.repos.d/HDP-UTILS.repo /etc/yum.repos.d/HDP-UTILS.repo.bak
yum update
```

Installation des dépendances système utiles :

```
yum install libyaml libxslt-devel libxml2-devel gcc 
yum install gcc-c++ cmake
yum install lapack blas
```

Installer les packages systèmes suivants : 

```
wget ftp://195.220.108.108/linux/centos/6.6/os/x86_64/Packages/lapack-devel-3.2.1-4.el6.x86_64.rpm
wget ftp://195.220.108.108/linux/centos/6.6/os/x86_64/Packages/blas-devel-3.2.1-4.el6.x86_64.rpm

yum install blas-devel-3.2.1-4.el6.x86_64.rpm
yum install lapack-devel-3.2.1-4.el6.x86_64.rpm
```

###Package python

Toutes les librairies se trouvent dans : `/home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf`

```
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####pypa-setuptools-da9a12631950.tar.gz

```
tar xzvf pypa-setuptools-da9a12631950.tar.gz
cd pypa-setuptools-da9a12631950
python setup.py build
cp -r build/lib/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####beautifulsoup4-4.3.2.tar.gz
```
tar xzvf beautifulsoup4-4.3.2.tar.gz
cd beautifulsoup4-4.3.2
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####lxml-3.3.6.tar.gz
```
tar xzvf lxml-3.3.6.tar.gz
cd lxml-3.3.6
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib.linux-x86_64-2.6 /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####Unidecode-0.04.17.tar.gz
```
tar xzvf Unidecode-0.04.17.tar.gz
cd Unidecode-0.04.17
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####requests-2.7.0.tar.gz
```
tar xzvf requests-2.7.0.tar.gz
cd requests-2.7.0
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####treetagger.tar.gz
```
cp treetaggerwrapper.py /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
tar xzvf treetagger.tar.gz
cp -r treetagger /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
cd /home/dbigdt/appli/bdf/dsf/Veille_eco/lib/treetagger/lib
cp english-utf8.par english.par 
```

####Cython-0.22.tar.gz
```
tar xzvf Cython-0.22.tar.gz
cd Cython-0.22
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib.linux-x86_64-2.6/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####numpy-1.9.2.tar.gz
```
tar xzvf numpy-1.9.2.tar.gz
cd numpy-1.9.2
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py install
```

####scipy-0.15.1.tar.gz
```
tar xzvf scipy-0.15.1.tar.gz
cd scipy-0.15.1
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib.linux-x86_64-2.6/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####scikit-learn-0.16.0
```
tar xzvf scikit-learn-0.16.0.tar.gz
cd scikit-learn-0.16.0
PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python setup.py build
cp -r build/lib.linux-x86_64-2.6/* /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

###Pour LDA

####mallet-2.0.7.tar.gz
```
tar xzvf mallet-2.0.7.tar.gz
cd mallet-2.0.7
yum install java-1.7.0-openjdk-devel
make clean
make
cd .. 
cp -r mallet-2.0.7 /home/dbigdt/appli/bdf/dsf/Veille_eco/lib
```

####package R

Créer le répertoire `/home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/R_package/` et récupérer les dépendances R :

```
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/R_package
cd /home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/R_package

wget http://cran.r-project.org/src/contrib/magrittr_1.5.tar.gz
wget http://cran.r-project.org/src/contrib/stringi_0.4-1.tar.gz
wget http://cran.r-project.org/src/contrib/stringr_1.0.0.tar.gz
wget http://cran.r-project.org/src/contrib/digest_0.6.8.tar.gz
wget http://cran.r-project.org/src/contrib/memoise_0.2.1.tar.gz
wget http://cran.r-project.org/src/contrib/chron_2.3-45.tar.gz
wget http://cran.r-project.org/src/contrib/Rcpp_0.11.6.tar.gz
wget http://cran.r-project.org/src/contrib/plyr_1.8.2.tar.gz
wget http://cran.r-project.org/src/contrib/reshape2_1.4.1.tar.gz
wget http://cran.r-project.org/src/contrib/data.table_1.9.4.tar.gz
wget http://cran.r-project.org/src/contrib/lubridate_1.3.3.tar.gz
wget http://cran.r-project.org/src/contrib/proxy_0.4-14.tar.gz
wget http://cran.r-project.org/src/contrib/tsne_0.1-2.tar.gz
wget http://cran.r-project.org/src/contrib/cluster_2.0.1.tar.gz
wget http://cran.r-project.org/src/contrib/foreach_1.4.2.tar.gz
wget http://cran.r-project.org/src/contrib/iterators_1.0.7.tar.gz
```

Dans le répertoire `/home/dbigdt/appli/bdf/dsf/Veille_eco/packages_bdf/R_package/`, lancer un client R pour installer les packages requis (attention, sur ce serveur a déjà été installé des packages pour shiny. Il est donc conseillé de jouer l'installation de shiny au cas où) :
```
install.packages("magrittr_1.5.tar.gz")
install.packages("stringi_0.4-1.tar.gz")
install.packages("stringr_1.0.0.tar.gz")
install.packages("digest_0.6.8.tar.gz")
install.packages("memoise_0.2.1.tar.gz")
install.packages("chron_2.3-45.tar.gz")
install.packages("Rcpp_0.11.6.tar.gz")
install.packages("plyr_1.8.2.tar.gz")
install.packages("reshape2_1.4.1.tar.gz")
install.packages("data.table_1.9.4.tar.gz")
install.packages("lubridate_1.3.3.tar.gz")
install.packages("proxy_0.4-14.tar.gz")
install.packages("tsne_0.1-2.tar.gz")
install.packages("cluster_2.0.1.tar.gz")
install.packages("iterators_1.0.7.tar.gz")
install.packages("foreach_1.4.2.tar.gz")
```

####LDA
Modifier le fichier `/home/dbigdt/appli/bdf/dsf/Veille_eco/src/LDA/main.sh` :
```
set -e
set -x

# capture stdin et stderr to a file
exec > >(tee logfile.txt)
exec 2>&1

pid=$!

echo generating the mallet input
#time ../mallet-2.0.7/bin/mallet svmlight --input data.svm \
/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/mallet-2.0.7/bin/mallet import-dir --input /home/dbigdt/appli/bdf/dsf/Veille_eco/work \
    --keep-sequence --remove-stopwords TRUE \
    --output /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/data.mallet

date
echo fitting the model
/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/mallet-2.0.7/bin/mallet train-topics \
    --num-topics 100 \
    --num-threads 7 \
    --random-seed 1 \
    --optimize-interval 20 \
    --num-iterations 100 \
    --output-model-interval 200 \
    --output-state-interval 200 \
    --input /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/data.mallet \
    --output-state /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/state.gz \
    --output-model /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/model \
    --output-doc-topics /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/doc_topics \
    --output-topic-keys /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/topic_keys \
    --topic-word-weights-file /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/word-weights-file \
    --word-topic-counts-file /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/word-topic-counts-file \
    --xml-topic-report /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/xml-topic-report \
    --xml-topic-phrase-report /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda/xml-topic-phrase-report 

kill $pid
```

Modifier le fichier `/home/dbigdt/appli/bdf/dsf/Veille_eco/src/LDA/kNN.R` pour remplacer la ligne suivante :
```
setwd("/home/Data_BDF/article_1000_1000iter/")
```
par
```
setwd("/home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda")
```
et modifier la valeur `path_original`
```
path_original<-"/home/dbigdt/appli/bdf/dsf/Veille_eco/out/"
```
par la valeur indiquée dans le fichier de configuration `lien_phys`.

Exemple :
```
path_original<-"/home/dbigdt/appli/bdf/dsf/Veille_eco/out/"
```


#Exécution

Tous les programmes se trouvent dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/src`

##Prérequis
```
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/out
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/work
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/fears
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/tmp
mkdir /home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda
```
##Fichier de configuration

Modifier le fichier `/home/dbigdt/appli/bdf/dsf/Veille_eco/src/config_bdf.py` avec les informations ci-dessous (pour l'encodage du mot de passe, voir : https://technodedigest.wordpress.com/2012/04/14/tackling-special-characters-in-proxy-passwords-on-linux/ ) :

```
# coding: utf-8

api_key_NYT = "2dd8341ea117fd4bb8e68c34240455bf:12:69413833"

output_dir = "/home/dbigdt/appli/bdf/dsf/Veille_eco/out/"

mysql ={'host' : "localhost", 'user':'', 'passwd':'', 'db':'BDF', 'sqlite_db':'/home/dbigdt/appli/bdf/dsf/Veille_eco/BDF.db'}

database_path= "/home/dbigdt/appli/bdf/dsf/Veille_eco/out/"

payload = {
    'username': 'bora.eang@bluestone.fr',
    'password': 'BLUESTONE'
}

proxy_user = "INTRA\BIGDATA_ASV"
proxy_password = "B%21gd%40t%402"
http_proxy = "Seraiauth.bdf.local:8080/"

treetagger_path = "/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/treetagger/"

preprocessed_data_path = "/home/dbigdt/appli/bdf/dsf/Veille_eco/tmp/"

lien_phys = "/home/dbigdt/appli/bdf/dsf/Veille_eco/out/"

fears_path = "/home/dbigdt/appli/bdf/dsf/Veille_eco/tmp/"

concept_analysis_path = "/home/dbigdt/appli/bdf/dsf/Veille_eco/tmp.out"

server_address = "lxdv291a.unix-int.intra-int.bdf-int.local"

csv_file_created_web_server = "/home/dbigdt/appli/bdf/dsf/Veille_eco/fears/file.csv"

recreated_doc_path = "/home/dbigdt/appli/bdf/dsf/Veille_eco/work/"
```

##Fichier js pour la phase FEARS

Créer un lien symbolique dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/src/WebInterface`
```
ln -s ../../fears/file.csv file.csv
```

##Exécution de la phase de scraping
```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python ScrapingNYTBDFV1.0.py -d 20150411 -f 20150412
```

Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/out`

```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python ScrapingBDFFT-base1.py
```

Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/out/FT*`

##Exécution de la phase de processing

```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PATH=$PATH:/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/treetagger PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python preprocessingBDF_base1.py
```

Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/tmp`

##Exécution de la phase FEARS
```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PATH=$PATH:/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/treetagger PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python FEARS_INDEX_and_ACP.py -d 20150411 -f 20150412
```

Suite à cette opération, un fichier json doit sortir sur la sortie standard.

##Démarrage du serveur web

```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PATH=$PATH:/home/dbigdt/appli/bdf/dsf/Veille_eco/lib/treetagger PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python BDF_HTTPServer.py 8000
```

##Exécution de la phase LDA

```
HTTP_PROXY_USERNAME="INTRA\BIGDATA_ASV" HTTP_PROXY_PASSWORD="B\!gd@t@2" HTTP_PROXY="http://Seraiauth.bdf.local:8080/" PYTHONPATH="/home/dbigdt/appli/bdf/dsf/Veille_eco/lib" python recreate_documents.py
```
Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/work`

```
./main.sh
```

Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda`

```
Rscript kNN.R
```

Suite à cette opération, des fichiers doivent apparaitre dans `/home/dbigdt/appli/bdf/dsf/Veille_eco/result_lda`

